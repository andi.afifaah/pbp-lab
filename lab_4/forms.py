from typing import Text
from django import forms
from lab_2.models import Note
from django.forms import ModelForm, widgets 

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = '__all__'
        widgets = {
            'From' : widgets.TextInput(attrs={'class':"form-control", 
            'style': 'width: 309px', 'placeholder': 'Dari'}),
            'to' : widgets.TextInput(attrs={'class':"form-control", 
            'style': 'width: 309px', 'placeholder': 'Untuk'}),
            'title':widgets.TextInput(attrs={'class':"form-control", 
            'style': 'width: 309px', 'placeholder': 'Judul'}),
            'message':widgets.Textarea(attrs={'class':"form-control", 'placeholder': 'Pesan'}),
        }
