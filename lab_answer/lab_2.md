1. Apakah perbedaan antara JSON dan XML?
    JSON merupakan singkatan dari "JavaScript Object Notation" sedangkan XML merupakan singkatan dari "Extensive Markup Language". Perbedaan mendasar dari keduanya adalah bahwa JSON memiliki format data interchange, sedangkan XML menyediakan format berupa markup language. Keduanya digunakan untuk delivery data, namun JSON sering digunakan untuk pengiriman data antar server dan browser sedangkan XML lebih sering dimanfaatkan untuk menyimpan data. JSON memiliki ukuran file yang lebih kecil dibandingkan XML namun hanya mendukung tipe data teks dan numerik, sedangkan XML mendukung tipe gambar dan tipe data lainnya.

2. Apakah perbedaan antara HTML dan XML?
    Perbedaan utama dari HTML dan XML adalah HTML digunakan untuk "display" data sehingga dikategorikan sebagai static sedangkan XML untuk transport dan penyimpanan data di database dan dikategorikan sebagai dynamic. Secara struktur, tag pada HTML digunakan untuk men-display data sedangkan pada XML untuk men-describe. Closing tag pada HTML pun tidak harus ada, sedangkan di XML harus ada. 

3. System API