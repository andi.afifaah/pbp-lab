import 'package:flutter/material.dart';

import '../dummy_data.dart';

class kontakDetailScreen extends StatelessWidget {
  static const routeName = '/kontak-detail';

  final Function toggleFavorite;
  final Function isFavorite;

  kontakDetailScreen(this.toggleFavorite, this.isFavorite);

  Widget buildSectionTitle(BuildContext context, String text) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Text(
        text,
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }

  Widget buildContainer(Widget child) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(10),
      height: 150,
      width: 300,
      child: child,
    );
  }

  @override
  Widget build(BuildContext context) {
    final kontakId = ModalRoute.of(context).settings.arguments as String;
    final selectedkontak =
        DUMMY_KONTAK.firstWhere((kontak) => kontak.id == kontakId);
    return Scaffold(
      appBar: AppBar(
        title: Text('${selectedkontak.namakontak}'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            buildSectionTitle(context, 'Alamat'),
            buildContainer(
              ListView.builder(
                itemBuilder: (ctx, index) => Card(
                  color: Theme.of(context).accentColor,
                  child: Padding(
                      padding: EdgeInsets.symmetric(
                        vertical: 5,
                        horizontal: 10,
                      ),
                      child: Text(selectedkontak.alamatkontak)),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          isFavorite(kontakId) ? Icons.star : Icons.star_border,
        ),
        onPressed: () => toggleFavorite(kontakId),
      ),
    );
  }
}
