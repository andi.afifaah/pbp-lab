import 'package:flutter/material.dart';

import '../models/kontak.dart';
import '../widgets/kontak_list.dart';

class FavoritesScreen extends StatelessWidget {
  final List<Kontak> favoriteKontak;

  FavoritesScreen(this.favoriteKontak);

  @override
  Widget build(BuildContext context) {
    if (favoriteKontak.isEmpty) {
      return Center(
        child: Text('Belum ada yang dipilih.'),
      );
    } else {
      return ListView.builder(
        itemBuilder: (ctx, index) {
          return KontakItem(
            id: favoriteKontak[index].id,
            category: favoriteKontak[index].category,
            namakontak: favoriteKontak[index].namakontak,
            nomorkontak: favoriteKontak[index].nomorkontak,
            region: favoriteKontak[index].region,
            provinsi: favoriteKontak[index].provinsi,
            kota: favoriteKontak[index].kota,
            alamatkontak: favoriteKontak[index].alamatkontak,
          );
        },
        itemCount: favoriteKontak.length,
      );
    }
  }
}
