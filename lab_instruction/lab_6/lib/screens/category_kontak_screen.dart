import 'package:flutter/material.dart';

import '../widgets/kontak_list.dart';
import '../models/kontak.dart';

class CategoryKontakScreen extends StatefulWidget {
  static const routeName = '/category-kontak';

  final List<Kontak> availableKontak;

  CategoryKontakScreen(this.availableKontak);

  @override
  _CategoryKontakScreenState createState() => _CategoryKontakScreenState();
}

class _CategoryKontakScreenState extends State<CategoryKontakScreen> {
  String categoryTitle;
  List<Kontak> displayedKontak;
  var _loadedInitData = false;

  @override
  void initState() {
    // ...
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_loadedInitData) {
      final routeArgs =
          ModalRoute.of(context).settings.arguments as Map<String, String>;
      categoryTitle = routeArgs['namakontak'];
      final categoryId = routeArgs['id'];
      displayedKontak = widget.availableKontak.where((kontak) {
        return kontak.category == categoryId;
      }).toList();
      _loadedInitData = true;
    }
    super.didChangeDependencies();
  }

  void _removeKontak(String kontakId) {
    setState(() {
      displayedKontak.removeWhere((kontak) => kontak.id == kontakId);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          //title: Text(categoryTitle),
          ),
      body: ListView.builder(
        itemBuilder: (ctx, index) {
          return KontakItem(
            id: displayedKontak[index].id,
            namakontak: displayedKontak[index].namakontak,
            nomorkontak: displayedKontak[index].nomorkontak,
            region: displayedKontak[index].region,
            provinsi: displayedKontak[index].provinsi,
            kota: displayedKontak[index].kota,
          );
        },
        itemCount: displayedKontak.length,
      ),
    );
  }
}
