import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/models/kontak.dart';

import './models/category.dart';
import './models/kontak.dart';

const DUMMY_CATEGORIES = const [
  Category(
    id: 'c1',
    title: 'Hotline COVID-19',
    color: Color.fromRGBO(0, 123, 2551, 1),
  ),
  Category(
    id: 'c2',
    title: 'Rumah Sakit',
    color: Color.fromRGBO(108, 117, 125, 1),
  ),
  Category(
    id: 'c3',
    title: 'Ambulans',
    color: Color.fromRGBO(255, 193, 7, 1),
  ),
  Category(
    id: 'c4',
    title: 'Bank Darah',
    color: Color.fromRGBO(220, 53, 69, 1),
  ),
  Category(
    id: 'c5',
    title: 'Suplier Alat Kesehatan',
    color: Color.fromRGBO(39, 167, 69, 1),
  ),
  Category(
    id: 'c6',
    title: 'Lainnya',
    color: Color.fromRGBO(52, 58, 64, 1),
  ),
];

const DUMMY_KONTAK = const [
  Kontak(
      id: 'k1',
      category: 'c1',
      region: 'Jawa',
      provinsi: 'Jawa Timur',
      kota: 'Surabaya',
      namakontak: 'Hotline COVID Pemkot Surabaya',
      alamatkontak: 'Jl. jalan ke kota',
      nomorkontak: '021 99999'),
  Kontak(
      id: 'k2',
      category: 'c2',
      region: 'Jawa',
      provinsi: 'Jawa Timur',
      kota: 'Surabaya',
      namakontak: 'RS Umum Surabaya',
      alamatkontak: 'Jl. jalan ke kota',
      nomorkontak: '021 99999'),
  Kontak(
      id: 'k3',
      category: 'c3',
      region: 'Jawa',
      provinsi: 'Jawa Timur',
      kota: 'Surabaya',
      namakontak: 'Ambulans Jasa Marga Surabaya',
      alamatkontak: 'Jl. jalan ke kota',
      nomorkontak: '021 99999'),
  Kontak(
      id: 'k4',
      category: 'c4',
      region: 'Jawa',
      provinsi: 'Jawa Timur',
      kota: 'Surabaya',
      namakontak: 'PMI Kota Surabaya',
      alamatkontak: 'Jl. jalan ke kota',
      nomorkontak: '021 99999'),
];
