import 'package:flutter/material.dart';

class Kontak {
  final String id;
  final String category;
  final String region;
  final String provinsi;
  final String kota;
  final String namakontak;
  final String alamatkontak;
  final String nomorkontak;

  const Kontak(
      {@required this.id,
      @required this.category,
      @required this.region,
      @required this.provinsi,
      @required this.kota,
      @required this.namakontak,
      @required this.alamatkontak,
      @required this.nomorkontak});
}
