import 'package:flutter/material.dart';

import '../screens/kontak_detail_screen.dart';
import '../models/kontak.dart';

class KontakItem extends StatelessWidget {
  final String id;
  final String category;
  final String region;
  final String provinsi;
  final String kota;
  final String namakontak;
  final String alamatkontak;
  final String nomorkontak;

  KontakItem(
      {@required this.id,
      @required this.category,
      @required this.region,
      @required this.provinsi,
      @required this.kota,
      @required this.namakontak,
      @required this.alamatkontak,
      @required this.nomorkontak});

  void selectKontak(BuildContext context) {
    Navigator.of(context)
        .pushNamed(
      kontakDetailScreen.routeName,
      arguments: id,
    )
        .then((result) {
      if (result != null) {
        // removeItem(result);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => selectKontak(context),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        elevation: 4,
        margin: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15),
                  ),
                ),
                Positioned(
                  bottom: 20,
                  right: 10,
                  child: Container(
                    width: 300,
                    color: Colors.black54,
                    padding: EdgeInsets.symmetric(
                      vertical: 5,
                      horizontal: 20,
                    ),
                    child: Text(
                      namakontak,
                      style: TextStyle(
                        fontSize: 26,
                        color: Colors.white,
                      ),
                      softWrap: true,
                      overflow: TextOverflow.fade,
                    ),
                  ),
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      SizedBox(
                        width: 18,
                      ),
                      Text(nomorkontak),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      SizedBox(
                        width: 18,
                      ),
                      Text(region),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      SizedBox(
                        width: 18,
                      ),
                      Text(provinsi),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      SizedBox(
                        width: 18,
                      ),
                      Text(kota),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
