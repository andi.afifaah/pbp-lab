import 'package:flutter/material.dart';

import './dummy_data.dart';
import './screens/tabs_screen.dart';
import './screens/kontak_detail_screen.dart';
import './screens/category_kontak_screen.dart';
import './screens/categories_screen.dart';
import './models/kontak.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<Kontak> _availablekontaks = DUMMY_KONTAK;
  List<Kontak> _favoritekontaks = [];

  void _toggleFavorite(String kontakId) {
    final existingIndex =
        _favoritekontaks.indexWhere((kontak) => kontak.id == kontakId);
    if (existingIndex >= 0) {
      setState(() {
        _favoritekontaks.removeAt(existingIndex);
      });
    } else {
      setState(() {
        _favoritekontaks.add(
          DUMMY_KONTAK.firstWhere((kontak) => kontak.id == kontakId),
        );
      });
    }
  }

  bool _iskontakFavorite(String id) {
    return _favoritekontaks.any((kontak) => kontak.id == id);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Daftar Makanan',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        accentColor: Colors.indigo,
        canvasColor: Color.fromRGBO(173, 232, 244, 1),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
            bodyText1: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            bodyText2: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            headline6: TextStyle(
              fontSize: 20,
              fontFamily: 'RobotoCondensed',
              fontWeight: FontWeight.bold,
            )),
      ),
      // home: CategoriesScreen(),
      initialRoute: '/', // default is '/'
      routes: {
        '/': (ctx) => TabsScreen(_favoritekontaks),
        CategoryKontakScreen.routeName: (ctx) =>
            CategoryKontakScreen(_availablekontaks),
        kontakDetailScreen.routeName: (ctx) =>
            kontakDetailScreen(_toggleFavorite, _iskontakFavorite),
      },
      onGenerateRoute: (settings) {
        print(settings.arguments);
      },
      onUnknownRoute: (settings) {
        return MaterialPageRoute(
          builder: (ctx) => CategoriesScreen(),
        );
      },
    );
  }
}
